import bpy
from mathutils import Matrix, Vector, Euler
from bpy.app.handlers import persistent
from math import pi

def get_now_kf(current_frame, fcurve):
    if fcurve is not None :
        for keyframe in fcurve.keyframe_points : 
            if keyframe.co.x == current_frame :
                return keyframe
    return None

def snap_parented_object(ob, target_matrix_world):
    #print('---------------------------------SNAP', ob.name, ob.users_scene) 
    #print('target')
    #print(target_matrix_world)  
    #determine parent
    bpy.context.view_layer.update()
    parent = None
    for c in ob.constraints :
        if c.type == 'CHILD_OF' and c.influence == 1 and c.target is not None:
            if c.target.type == 'ARMATURE' and c.subtarget != '' :
                parent = c.target.pose.bones[c.subtarget]
                inv_parent = Matrix(c.target.matrix_world @ parent.matrix)
            else :
                parent = c.target
                inv_parent = Matrix(parent.matrix_world)
            break
    if parent is None :
        inv_parent = Matrix()  
    
    #compute result
    else :
        #print('PARENT = ', parent.name)
        
        inv_parent.invert()  
    result = Matrix(inv_parent @ target_matrix_world)
    #assign
    
    assign_matrix_basis(ob, result, bpy.context.scene.frame_current)
    #bpy.context.view_layer.update()
    #print('result')
    #print(ob.matrix_world)
    return

def snap_parented_armature(ob, target_matrix_world):
    #print('---------------------------------SNAP', ob.name, ob.users_scene) 
    #print('target')
    #print(target_matrix_world)  
    #determine parent
    bpy.context.view_layer.update()
    parent = None
    for c in ob.constraints :
        if c.type == 'CHILD_OF' and c.influence == 1 and c.target is not None:
            if c.target.type == 'ARMATURE' and c.subtarget != '' :
                parent = c.target.pose.bones[c.subtarget]
                inv_parent = Matrix(c.target.matrix_world @ parent.matrix)
            else :
                parent = c.target
                inv_parent = Matrix(parent.matrix_world)
            break
    if parent is None :
        inv_parent = Matrix()  
    
    #compute result
    else :
        #print('PARENT = ', parent.name)
        
        inv_parent.invert()  
    result = Matrix(inv_parent @ target_matrix_world)
    #assign
    ob.location = result.to_translation()
    #assign_matrix_basis(ob, result, bpy.context.scene.frame_current)
    #bpy.context.view_layer.update()
    #print('result')
    #print(ob.matrix_world)
    return
   
def assign_matrix_basis(ob, matrix, frame_nb):
    loc, rot, sca = matrix.decompose()
    #print('assign to', ob.name, loc, rot.to_euler(), sca)

    ob.location = loc
    ob.rotation_euler = rot.to_euler()
    ob.scale = sca

    a= ob.keyframe_insert(data_path ='location', index = -1, frame = frame_nb)
    b= ob.keyframe_insert(data_path ='rotation_euler', index = -1, frame = frame_nb)
    c= ob.keyframe_insert(data_path ='scale', index = -1, frame = frame_nb)  
    #print(a,b,c) 
     
def get_next_keyframe(instant, fcurves) :
    'returns from fc list  the frame number of the next keyframe after the instant'
    result = 999999
    for fcurve in fcurves :
        for kf in fcurve.keyframe_points :
            if kf.co.x > instant :
                if kf.co.x < result :
                    result = int(kf.co.x)
                break
    if result != 999999 :
        return result
    else : return None

unik_prefix = '.'

def get_unik_level(name,prefix=unik_prefix):
    level= 0
    while name[level] == prefix:
        level+=1
    return level

def get_unik_prefix(name,prefix=unik_prefix):
    level= get_unik_level(name,prefix)    
    return name[:level]

class INFRONT_OT_TOGGLE(bpy.types.Operator):
    bl_idname = "ctrl.in_front_toggle"
    bl_label = "Enable In front on current rig"
    bl_options = {'UNDO'}
    mode : bpy.props.BoolProperty(default = True)
    select : bpy.props.StringProperty(default = 'RIG')
    def execute(self, context) :
        if self.select == 'RIG':
            rig_collection = bpy.data.collections['COL_RIG_' +context.object.name.split('_')[2], None]
            objects = rig_collection.all_objects
        if self.select == 'COLLECTION' :
            objects = context.collection.all_objects
        for ob in objects :
            ob.show_in_front = self.mode     
        return {'FINISHED'}
class ALLCTRLVIS_OT_TOGGLE(bpy.types.Operator):
    bl_idname = "ctrl.all_toggle_visibility"
    bl_label = "Toggle all controllers visibility"
    bl_options = {'UNDO'}
    def execute(self, context) :
        scene = context.scene
        
        for ob in [o for o in scene.objects if o.type == 'GPENCIL'] :
            if 'MASTER_C' in ob.name and 'CTRL_VISIBILITY' in ob.keys():
                if ob.animation_data and ob.animation_data.action and '["CTRL_VISIBILITY"]' in [fc.data_path for fc in ob.animation_data.action.fcurves]: 
                    for fc in ob.animation_data.action.fcurves :
                        if fc.data_path == '["CTRL_VISIBILITY"]' :
                            ob.animation_data.action.fcurves.remove(fc)
                if scene.show_all_ctrl :ob['CTRL_VISIBILITY'] = 1
                else : ob['CTRL_VISIBILITY'] = 0
            for mod in ob.grease_pencil_modifiers :
                if mod.type == 'GP_COLOR' and mod.show_render == False :
                    if scene.show_all_ctrl :mod.show_viewport = True
                    else : mod.show_viewport = False
            ob.location = ob.location
        if scene.show_all_ctrl: scene.show_all_ctrl = False
        else : scene.show_all_ctrl = True
        
        #force refresh drivers
        fr = context.scene.frame_current
        context.scene.frame_set(fr +1)
        context.scene.frame_set(fr)  
        
        return {'FINISHED'}

class CTRLVIS_OT_TOGGLE(bpy.types.Operator):
    bl_idname = "ctrl.toggle_visibility"
    bl_label = "Toggle controllers visibility"
    bl_options = {'UNDO'}
    def execute(self, context) :
        prefix = get_unik_prefix(context.object.name)

        master = bpy.data.objects[ prefix+ 'CTLR_RIG_' + context.object.name.split('_')[2] + "_MASTER_C", None]
        if master['CTRL_VISIBILITY'] == 1 :
            master['CTRL_VISIBILITY'] = 0
        else :
            master['CTRL_VISIBILITY'] = 1

        if context.scene.tool_settings.use_keyframe_insert_auto and '["CTRL_VISIBILITY"]' in [fc.data_path for fc in master.animation_data.action.fcurves] :    
            master.keyframe_insert('["CTRL_VISIBILITY"]', frame = context.scene.frame_current)
            #force refresh drivers
            fr = context.scene.frame_current
            context.scene.frame_set(fr +1)
            context.scene.frame_set(fr)  
        else : #force refresh
            master.location = master.location
            pass 
        return {'FINISHED'}


class FREE_OT_TOGGLE(bpy.types.Operator):
    bl_idname = "free.toggle"
    bl_label = "switch between Free or attached"
    bl_options = {'UNDO'}
    def execute(self, context) :
        if 'gpm_scene' in context.scene.keys() :
            context.scene.gpm_scene.lock_handler = True
        bpy.context.view_layer.update()
        ob = context.object
        transforms = ['location', 'rotation_euler', 'scale']

        next_kf =  get_next_keyframe(context.scene.frame_current, ob.animation_data.action.fcurves)
        if next_kf :
            fr = context.scene.frame_current
            context.scene.frame_set(next_kf)       
            ob.keyframe_insert(data_path = '["FREE"]', frame = next_kf)
            for transform in transforms :
                ob.keyframe_insert(data_path = transform, index =-1, frame = next_kf)
            context.scene.frame_set(fr)

        #print('-------TOGGLE FREE')
        target = Matrix(ob.matrix_world)
        if ob['FREE'] == 0 :
            #print('LINKED ==> FREE')
            ob['FREE'] = 1
        else : 
            #print('FREE ==> LINKED')
            ob['FREE'] = 0
        if context.scene.tool_settings.use_keyframe_insert_auto :  
            ob.keyframe_insert('["FREE"]', frame = context.scene.frame_current)
            #force refresh drivers
            fr = context.scene.frame_current
            context.scene.frame_set(fr +1)
            context.scene.frame_set(fr)    
        
        snap_parented_object(ob, target)
        print('done')
        if 'gpm_scene' in context.scene.keys() :
            context.scene.gpm_scene.lock_handler = False
        return {'FINISHED'}

class IKFK_OT_CLEAR(bpy.types.Operator):
    '''switch the whole scene into FK'''
    bl_idname = "ikfk.clear"
    bl_label = "switch the whole scene into FK"
    bl_options = {'UNDO'}
    
    def execute(self, context) :
        init_cur_fr = context.scene.frame_current
        ik_roots = []
        for ob in context.scene.objects : 
            if ob.ik_root and ob not in ik_roots:
                ik_roots.append(ob.ik_root)
        #print([root.name for root in ik_roots])
        for root in ik_roots :
            
            ikfk_fc = [fc for fc in root.animation_data.action.fcurves if fc.data_path == '["IK_FK"]'][0]

            max = 99999
            ik_intervals = []
            ik_start = -max
            for kf in ikfk_fc.keyframe_points :
                if kf.co.y == 1 and ik_start == -max: #ik is turned on
                    ik_start = int(kf.co.x)
                if kf.co.y == 0 and ik_start != -max : #ik is turned off
                    ik_intervals.append((ik_start, int(kf.co.x)))
                    ik_start = -max
            if ik_start != -max : #ik is turned on at the end of the timeline
                ik_intervals.append((ik_start, max))
            
            fcs = []
            for part in [p for p in root.ik_parts if p.structural] :
                for fc in part.controller.animation_data.action.fcurves :
                    if fc.data_path in ['location', 'roration_euler', 'scale']:
                        fcs.append(fc)   
            
            frames = []
            for fc in fcs :
                for kf in fc.keyframe_points :
                    for interval in ik_intervals :
                        a = interval[0]
                        b = interval[1]
                        if kf.co.x in range(a,b) :
                            if int(kf.co.x) not in frames :
                                frames.append(int(kf.co.x))
                            break

            for frame in reversed(frames):
                context.scene.frame_set(frame)
                bpy.ops.ikfk.toggle(ob_name = root.name)
        context.scene.frame_set(init_cur_fr)
        return {'FINISHED'}

class IKFK_OT_TOGGLE(bpy.types.Operator):
    '''Switch between IK and FK animation mode'''
    bl_idname = "ikfk.toggle"
    bl_label = "switch between IK or FK"
    bl_options = {'UNDO'}
    ob_name : bpy.props.StringProperty()
    def execute(self, context) :
        if self.ob_name == '' :
            ob = context.object.ik_root
        else : 
            ob = bpy.data.objects[self.ob_name]    
        if not ob :
            self.report({'INFO'}, message = 'No IK root found from this object')
            return {'CANCELLED'}
        #print('-------TOGGLE IKFK')
        if 'gpm_scene' in context.scene.keys() :
            try:
                context.scene.gpm_scene.lock_handler = True
            except : pass
        #find the next keyframe on the limb and keyframe everything
        transforms = ['location', 'rotation_euler', 'scale']
        moving_obs = []
        for part in [p for p in ob.ik_parts if p.structural] :
            moving_obs.append(part.controller)
            moving_obs.append(part.drawing)
        fcs = []
        for moving_ob in moving_obs :    
            for fc in [fc for fc in moving_ob.animation_data.action.fcurves if fc.data_path in transforms] :
                fcs.append(fc)    

        next_kf =  get_next_keyframe(context.scene.frame_current, fcs)
        if next_kf :
            fr = context.scene.frame_current
            context.scene.frame_set(next_kf)
                    
            ob.keyframe_insert(data_path = '["IK_FK"]', frame = next_kf)
            for moving_ob in moving_obs :
                for transform in transforms :
                    moving_ob.keyframe_insert(data_path = transform, index =-1, frame = next_kf)
            context.scene.frame_set(fr)


        if ob['IK_FK'] == 0 : #FK to IK ------------------------------------------
            #print('==> FK to IK ==>++++')

            for i, part in enumerate(ob.ik_parts):
                if part.structural:
                    if 'IK_FLIP' in part.controller.keys() :
                        flip_ctrl = part.controller
                        v1 = ob.ik_parts[i-1].drawing.matrix_world.to_translation() - part.drawing.matrix_world.to_translation()
                        v1_2d = Vector((v1[0], v1[2]))
                        v2 = ob.ik_parts[i+1].drawing.matrix_world.to_translation() - part.drawing.matrix_world.to_translation()
                        v2_2d = Vector((v2[0], v2[2]))
                        angle = (v1_2d).angle_signed(v2_2d)
                        #print('-------------------------------------------ANGLE',angle)
                        if angle > 0 :
                            flip = 1
                        else : 
                            flip = 0
                
                        flip_ctrl['IK_FLIP'] = flip
                        flip_ctrl.keyframe_insert('["IK_FLIP"]', frame = context.scene.frame_current)
                        #force refresh drivers
                        fr = context.scene.frame_current
                        context.scene.frame_set(fr +1)
                        context.scene.frame_set(fr)
                        break
                
                #snap controller
            #for part in reversed(ob.ik_parts): 
            for i in [0,2,1] :
                part = ob.ik_parts[i] 
                #get target matrix with only location xz
                targetloc = Vector(part.drawing.matrix_world.to_translation())
                #targetloc[1] = 0 
                target =  Matrix.LocRotScale(targetloc, Euler(), Vector((1,1,1)))
                snap_parented_object(part.controller, target) #snap controllers 
                part.controller.location[1] = 0 #but keep y axis to zero
                part.controller.keyframe_insert(data_path ='location', index = 1, frame = context.scene.frame_current)
                
                part.controller.rotation_euler = Euler((0,0,0))
                part.controller.scale = Vector((1,1,1))
                
                part.controller.keyframe_insert(data_path ='rotation_euler', index = -1, frame = context.scene.frame_current)
                part.controller.keyframe_insert(data_path ='scale', index = -1, frame = context.scene.frame_current)
                
                #snap_parented_object(part.controller, part.drawing.matrix_world) #snap controllers 

            snap_drawings = []
            for i, part in enumerate(ob.ik_parts) :
                if part.drawing and part.structural :
                    snap_drawings.append((part.drawing, Matrix(part.drawing.matrix_world)))

            ob['IK_FK'] = 1 #activate driven props  
            ob.keyframe_insert('["IK_FK"]', frame = context.scene.frame_current)
            
            #force refresh drivers
            fr = context.scene.frame_current
            context.scene.frame_set(fr +1)
            context.scene.frame_set(fr)
            
            for drawing, target in snap_drawings :
                if not ('FREE' in drawing.keys() and drawing['FREE'] == 1):
                    
                    snap_parented_object(drawing, target)
        else : #IK to FK -------------------------------------------------------------
            #print('==> IK to FK ==>')
            context.view_layer.update()
            snap_list = []
            for part in [p for p in ob.ik_parts if p.structural]: #store coordinztes first
                if not ('FREE' in part.drawing.keys() and part.drawing['FREE'] == 1):
                    target = Matrix(part.drawing.matrix_world)
                    snap_list.append((part.drawing,target))

            ob['IK_FK'] = 0 #activate driven prop 
            ob.keyframe_insert('["IK_FK"]',frame = context.scene.frame_current)
            context.view_layer.update() 

            #force refresh drivers
            fr = context.scene.frame_current
            context.scene.frame_set(fr +1)
            context.scene.frame_set(fr)
            
            for obj, target in snap_list :                  
                snap_parented_object(obj, target) #assign new coordinates
        #print('done')
        if 'gpm_scene' in context.scene.keys() :
            context.scene.gpm_scene.lock_handler = False
        return {'FINISHED'}
  
class RIGSETUP_IKPART(bpy.types.PropertyGroup):
    bl_label = "IK part properties"
    bl_idname = "rigsetup_ikpart"

    drawing : bpy.props.PointerProperty(type = bpy.types.Object)
    structural : bpy.props.BoolProperty(default = True)
    controller : bpy.props.PointerProperty(type = bpy.types.Object)
    armature : bpy.props.PointerProperty(type = bpy.types.Object)
    holder_name : bpy.props.StringProperty() 

class IKSET_OT_ADDPART(bpy.types.Operator):
    bl_label = "Add an IK part "
    bl_idname = "rigsetup.addikpart"
    def execute(self, context): 
        context.object.ik_parts.add()
        return {'FINISHED'}
    
class IKSET_OT_DELPART(bpy.types.Operator):
    bl_label = "Remove an IK part "
    bl_idname = "rigsetup.delikpart"
    
    part_index : bpy.props.IntProperty()
    def execute(self, context): 
        context.object.ik_parts.remove(self.part_index)
        return {'FINISHED'}

class IKSET_OT_FINISH(bpy.types.Operator):
    '''Finish ik setup (set ik root props)
    Hold ctrl for auto armature setup '''
    bl_label = "finish IK setup"
    bl_idname = "rigsetup.finish"
    def invoke(self, context, event):
        if event.ctrl :
            #snap armature to ik_root
            parts = context.object.ik_parts
            arma = parts[0].armature
            bpy.context.view_layer.update()
            snap_parented_armature(arma, context.object.matrix_world)
            y_world = context.object.matrix_world.to_translation()[1]

            #adjust armature to drawings (edit mode)
            arma.hide_select = False
            arma.hide_viewport = False
            context.view_layer.objects.active = arma
            bpy.ops.object.mode_set(mode='EDIT')
            
            bone_dict = {}
            atriculation_bones = ['articulation', 'preholder_2', 'ik1_parent']#, 'holder_2']
            articulation_target = parts[1].drawing.matrix_world.to_translation()
            articulation_target[1] = y_world
            for bname in atriculation_bones :
                b = arma.data.edit_bones[bname]
                bone_dict[b] = articulation_target

            extremity_bones = ['extremity', 'ik3', 'preholder_3']#, 'holder_3']
            extremity_target = parts[2].drawing.matrix_world.to_translation()
            extremity_target[1] = y_world
            for bname in extremity_bones :
                b = arma.data.edit_bones[bname]
                bone_dict[b] = extremity_target

            ######### for debug
            #print(bone_dict)
            #bone_dict = {arma.data.edit_bones['Bone']: articulation_target}
            ##################
            arma_inv = Matrix(arma.matrix_world)
            arma_inv.invert()

            for bone in bone_dict.keys() :
                print(bone.name)
                target = bone_dict[bone]   
                local_target = arma_inv @ target
                orient_tail = bone.tail - bone.head
                bone.head = local_target
                bone.tail = bone.head + orient_tail
            
            arma.data.edit_bones['2d1'].tail = arma_inv @ articulation_target
            arma.data.edit_bones['2d2'].head = arma_inv @ articulation_target
            arma.data.edit_bones['2d2'].tail = arma_inv @ extremity_target

            v1 = parts[0].drawing.matrix_world.to_translation() - parts[1].drawing.matrix_world.to_translation()
            v1_2d = Vector((v1[0], v1[2]))
            v2 = parts[2].drawing.matrix_world.to_translation() - parts[1].drawing.matrix_world.to_translation()
            v2_2d = Vector((v2[0], v2[2]))
            angle = (v1_2d).angle_signed(v2_2d)
            print('-------------------------------------------ANGLE',angle)
            if angle > 0 and arma.matrix_world.to_scale()[0] < 0 or angle < 0 and arma.matrix_world.to_scale()[0] > 0 :
                arma.pose.bones['ik3'].constraints['IK'].pole_angle = -pi/2
            else : 
                arma.pose.bones['ik3'].constraints['IK'].pole_angle = pi/2
            
            for b in arma.data.edit_bones :
                b.head += Vector()

            #arma.data.edit_bones.update()
            #arma.update_tag()
            bpy.ops.object.mode_set(mode='OBJECT')
            context.view_layer.objects.active = parts[0].drawing
            arma.hide_select = True
            arma.hide_viewport = True

        #write ik_root pointers on drawings en controllers
        for part in context.object.ik_parts :            
            part.drawing.ik_root = context.object
            if part.structural :
                part.controller.ik_root = context.object    
        #bpy.context.view_layer.update()        
        
        return {'FINISHED'}

class OBJECT_OT_FLIP(bpy.types.Operator):
    bl_label = "Flip an object from custom prop "
    bl_idname = "rigctrl.flip_obj"
    bl_options = {'UNDO'}
    
    def execute(self, context): 
        if context.object['FLIP'] == 0 :
           context.object['FLIP'] = 1
        else :
            context.object['FLIP'] = 0
        context.object.keyframe_insert('["FLIP"]', frame = context.scene.frame_current)
        #force refresh drivers
        fr = context.scene.frame_current
        context.scene.frame_set(fr +1)
        context.scene.frame_set(fr)
        return {'FINISHED'} 

class OBJECT_OT_IKFLIP(bpy.types.Operator):
    bl_label = "Flip bending direction of the ik system from custom prop "
    bl_idname = "rigctrl.ik_flip"
    bl_options = {'UNDO'}
    ob_name : bpy.props.StringProperty()
    def execute(self, context): 
        if self.ob_name != '' :
            ob = bpy.data.objects[self.ob_name]
            if ob['IK_FLIP'] == 0 :
                ob['IK_FLIP'] = 1
            else :
                ob['IK_FLIP'] = 0
            ob.keyframe_insert('["IK_FLIP"]', frame = context.scene.frame_current)
            #force refresh drivers
            fr = context.scene.frame_current
            context.scene.frame_set(fr +1)
            context.scene.frame_set(fr)
            return {'FINISHED'} 
        
class RIGSETUP_QS(bpy.types.PropertyGroup) :
    bl_label = "quick select object pointer"
    bl_idname = "rigsetup_qs"
    name : bpy.props.StringProperty()
    object : bpy.props.PointerProperty(type = bpy.types.Object)

class RIGSETUP_QUICK_SELS(bpy.types.PropertyGroup) :
    bl_label = "quick select objects list"
    bl_idname = "rigsetup_quicksels"
    objects : bpy.props.CollectionProperty(type = RIGSETUP_QS)
    active_index : bpy.props.IntProperty(default = 0)

class RIGSETUP_OT_ADDSEL_QS(bpy.types.Operator):
    bl_label = "add selected objects to active object's quick select"
    bl_idname = "rigsetup.add_sel_to_qs"
    bl_options = {"UNDO", "REGISTER"}
    def execute(self, context): 
        for ob in context.selected_objects :
            if ob != context.object :
                new_qs = context.object.quick_selects.objects.add()
                new_qs.object = ob
                new_qs.name = ob.name
        return {'FINISHED'}

def is_ok(o):
    if o in list(bpy.context.view_layer.objects):
        return o.type == 'GPENCIL' and not (o.hide_get() or o.hide_viewport)
    return False

@persistent        
def show_qs_handler(scene, self):
    if scene.show_qs_on == True:
        for ob in scene.objects :
            if 'SHOW_QS_TINT' in ob.grease_pencil_modifiers.keys() : 
                tint = ob.grease_pencil_modifiers['SHOW_QS_TINT']
                if ob in [o.object for o in bpy.context.object.quick_selects.objects] :
                    tint.color = (1,1,0)
                    tint.factor = 0.5
                elif bpy.context.object in [o.object for o in ob.quick_selects.objects]:
                    tint.color = (0,1,1)
                    tint.factor = 0.5
                else :
                    tint.factor = 0


class RIGSETUP_OT_SHOW_QS(bpy.types.Operator) :
    bl_label = "show quick selects of active object"
    bl_idname = "rigsetup.show_qs"
    def turn_on(self, context): 
        for o in context.scene.objects : 
            if is_ok(o):
                tint = o.grease_pencil_modifiers.new('SHOW_QS_TINT', 'GP_TINT')
                tint.color = (1,1,0)
                tint.factor = 0
        context.scene.show_qs_on = True
        bpy.app.handlers.depsgraph_update_post.append(show_qs_handler)
        return {'FINISHED'}

    def turn_off(self, context): 
        for o in context.scene.objects :
            if o.type == 'GPENCIL': 
                for mod in o.grease_pencil_modifiers :
                    if 'SHOW_QS_TINT' in mod.name :
                        o.grease_pencil_modifiers.remove(mod)
        context.scene.show_qs_on = False
        bpy.app.handlers.depsgraph_update_post.remove(show_qs_handler)
        return {'FINISHED'}

    def execute(self, context): 
        if context.scene.show_qs_on == False :
            self.turn_on(context)
        else :
            self.turn_off(context)
        return {'FINISHED'}



class RIGSETUP_OT_ADD_QS(bpy.types.Operator):
    bl_label = "add object to active object's quick select"
    bl_idname = "rigsetup.add_to_qs"
    bl_options = {"UNDO", "REGISTER"}
    object_name : bpy.props.StringProperty()
   
    def execute(self, context): 
        ob = bpy.data.objects[self.object_name]
        if ob != context.object :
            new_qs = context.object.quick_selects.objects.add()
            new_qs.object = ob
            new_qs.name = ob.name
        return {'FINISHED'}
    
class RIGSETUP_OT_REMOVE_QS(bpy.types.Operator):
    bl_label = "remove object from quick select "
    bl_idname = 'rigsetup.remove_qs'
    bl_options = {'REGISTER','UNDO'}    
    i : bpy.props.IntProperty()   

    def execute(self, context): 
        #qs = context.object.quickselects.objects[self.i]
        context.object.quick_selects.objects.remove(context.object.quick_selects.active_index)
        return{'FINISHED'}
    
class QS_OT_SELECT(bpy.types.Operator):
    bl_label = "quick select object"
    bl_idname = "qs.select"
    object_name : bpy.props.StringProperty()

    def execute(self, context): 
        for ob in context.selected_objects :
            ob.select_set(False)
        bpy.data.objects[self.object_name].select_set(True)
        context.view_layer.objects.active = bpy.data.objects[self.object_name]
        return {'FINISHED'}
    
class QV_OT_SELECT(bpy.types.Operator):
    '''Quickly select vertex groups.
    Ctrl + clic to directly switch in edit mode'''
    bl_label = "quick select vertices"
    bl_idname = "qv.select"
    vertex_group_index : bpy.props.IntProperty()
    ctrl : bpy.props.BoolProperty()
    def invoke(self,context,event):
        self.ctrl = event.ctrl
        self.execute(context)
        return {'FINISHED'}
    def execute(self, context): 
        if self.ctrl :
            bpy.ops.object.mode_set(mode = 'EDIT_GPENCIL')
        o = context.object
        if o.type == 'GPENCIL' :
            strokes = []
            for layer in o.data.layers :
                frame = layer.active_frame
                for stroke in frame.strokes :
                    try : 
                        stroke.points.weight_get(vertex_group_index=self.vertex_group_index, point_index=0)
                        strokes.append(stroke)
                    except Exception as e :
                        #print(e)
                        pass                   
            for stroke in strokes :
                for i,p in enumerate(stroke.points) :
                    weight = stroke.points.weight_get(vertex_group_index=self.vertex_group_index, point_index=i)
                    if weight >= 0 :
                        #print("Vertex #%d in the group (w=%d)."%(i, weight))
                        p.select = True
                    else : 
                        #print("Vertex #%d not in the group (w=%d)."%(i, weight))
                        p.select = False
            
        return {'FINISHED'}
