# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    "name" : "Ewilan Rig Setup/Control",
    "author" : "Tom VIGUIER",
    "description" : "",
    "blender" : (3, 6, 0),
    "version" : (0, 0, 4),
    "location" : "",
    "category" : "Andarta"
}

from .Operators import *
from .select_parent import *
import rna_keymap_ui


class QS_OT_ADD_MENU(bpy.types.Operator): #choose a layer in the menu list
    bl_label = "Add Object"
    bl_idname = "qs.add_menu"
    i : bpy.props.IntProperty()

    def execute(self, context):
        context.window_manager.gpm_window.gpm_i = self.i
        context.window_manager.popup_menu(draw_add_menu)
        return{'FINISHED'}
    
class RIGSETUP_PT_PANEL(bpy.types.Panel):
    bl_label = "Rig Setup"
    bl_idname = "RIGSETUP_PT_PANEL"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Andarta"
    bl_options = {"DEFAULT_CLOSED"}

    def draw(self, context):
        preferences = context.preferences
        addon_prefs = preferences.addons[__name__].preferences
        
        layout = self.layout
        row = layout.row()
        if addon_prefs.enable_setup :
            if context.object is not None:
                row.label(text = context.object.name)
                for i, part in enumerate(context.object.ik_parts) :
                    row = layout.row()
                    row.prop(part, 'name')
                    ops = row.operator('rigsetup.delikpart', text = 'X')
                    ops.part_index = i                    
                    row = layout.row()
                    row.prop(part, 'drawing')
                    row.prop(part, 'structural', text = '')
                    if part.structural :
                        row = layout.row()
                        row.prop(part, 'controller')
                        row = layout.row()
                        row.prop(part, 'armature')
                        if part.armature and part.armature.type == 'ARMATURE' : 
                            row = layout.row()
                            row.prop_search(part, 'holder_name', part.armature.data, "bones", text="Bone")
                    '''for item in dir(part) : 
                        if item not in ['__annotations__', '__dict__', '__doc__', '__module__', '__weakref__', 'bl_idname', 'bl_label', 'bl_rna', 'rna_type', 'name'] :
                            row = layout.row()
                            row.prop(part, item)'''
                row = layout.row()
                row.operator('rigsetup.addikpart')
                row = layout.row()
                row.operator('rigsetup.finish')
                row = layout.row()
                row.label(text='Quick select : ')
                row = layout.row()
                col = row.column()
                col.template_list("RIGSETUP_UL_QS", "", context.object.quick_selects, "objects", context.object.quick_selects, "active_index", rows=4, sort_lock=True)
                col2 = row.column(align=True)   
                col2.operator("qs.add_menu", icon='ADD', text="")
                col2.operator("rigsetup.remove_qs", icon='REMOVE', text="")
                col2.operator("rigsetup.show_qs", icon='HIDE_OFF', text="", depress=context.scene.show_qs_on)
                col2.operator("rigsetup.add_sel_to_qs", icon='PLUS', text="")

        else :
            row.label(text = 'Setup panel disabled from add-on preferences')
            

def draw_add_menu(self, context):

    layout = self.layout
    ob = context.object

    done = False
    for ob in context.scene.objects:
        if ob != context.object and ob not in [ob.object for ob in context.object.quick_selects.objects]: 
            done = True
            add = layout.operator("rigsetup.add_to_qs", text=ob.name)
            add.object_name = ob.name

    if done is False:
        layout.label(text="No layers to add")


class RIGCONTROL_PT_PANEL(bpy.types.Panel):
   
    bl_label = "Rig Control"
    bl_idname = "RIGCTRL_PT_PANEL"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Andarta"

    def draw(self, context):
        layout = self.layout
        row = layout.row()

        if not context.scene.show_all_ctrl :
            text = 'Hide all controllers'
            icon = 'HIDE_OFF'
        else : 
            text = 'Show all controllers'
            icon = 'HIDE_ON'
        row.operator('ctrl.all_toggle_visibility', text =text, depress=not(context.scene.show_all_ctrl), icon=icon)
    
        row = layout.row()
        row.operator('ikfk.clear', text = 'Clear IK')
        box = layout.box()
        row = box.row()
        
        if context.object :
            if '_' in context.object.name and len(context.object.name.split('_')) > 2 :
                prefix = get_unik_prefix(context.object.name)
                master_name = prefix +'CTLR_RIG_' + context.object.name.split('_')[2] + "_MASTER_C" 
                if master_name in bpy.data.objects.keys() :
                    master = bpy.data.objects[master_name]
                    row.label(text = context.object.name.split('_')[2])
                    row = box.row()
                    row.label(text = 'In front : ')
                    ops = row.operator('ctrl.in_front_toggle', text = 'ON')
                    ops.mode = True
                    ops = row.operator('ctrl.in_front_toggle', text = 'OFF')
                    ops.mode = False
                    
                    if 'CTRL_VISIBILITY' in master.keys() :
                        if master['CTRL_VISIBILITY'] == 0 :
                            icon = 'HIDE_ON'
                        else : 
                            icon = 'HIDE_OFF'
                        
                        row = box.row()  
                        row.operator('ctrl.toggle_visibility', icon = icon)
                
                    if 'CTRL_THICKNESS' in master.keys() :
                        row = box.row()  
                        row.prop(master, '["CTRL_THICKNESS"]', text = 'Line Thickness')
             
                    box2 = box.box()
                    row = box2.row()  
                    row.label(text = 'ITEM : ' + ' '.join(context.object.name.split('_')[3:]))
                    if 'FREE' in context.object.keys() :
                        if context.object['FREE'] == 0 :
                            icon = 'SNAP_ON'
                        else : 
                            icon = 'SNAP_OFF'
                        row = box2.row()   
                        row.operator('free.toggle', icon = icon)

                    root = context.object.ik_root
                    if root :
                        if root['IK_FK'] == 0 :
                            icon2 = 'POSE_HLT'
                        else : 
                            icon2 = 'OUTLINER_OB_ARMATURE'
                        #row.label(text = mode)
                        row = box2.row()   
                        row.operator('ikfk.toggle', icon = icon2)
                            
                        if root['IK_FK'] == 1 :
                            for part in root.ik_parts :
                                if part.structural and 'IK_FLIP' in part.controller : 
                                    row = box2.row() 
                                    ops = row.operator('rigctrl.ik_flip', text = 'FLIP Articulation', icon = 'MOD_MIRROR')
                                    ops.ob_name = part.controller.name
                    if 'FLIP' in context.object.keys() :
                        row = box2.row() 
                        row.operator('rigctrl.flip_obj', text = 'FLIP', icon = 'MOD_MIRROR')
                    
                box = layout.box()     
                if len(context.object.quick_selects.objects) > 0 :  
                    row = box.row()
                    row.label(text = 'Quick select : ')
                    for obj in context.object.quick_selects.objects :
                        if obj.object :
                            row = box.row() 
                            ops = row.operator('qs.select', text = ' '.join(obj.object.name.split('_')[3:]))
                            ops.object_name = obj.object.name
                
                if len(context.object.vertex_groups) > 0 :
                    row = box.row()
                    row.label(text= 'Quick vertex groups :')
                    row = box.row()
                    for i, vg in enumerate(context.object.vertex_groups) :                            
                        if vg.name.startswith('SEL_') and len(vg.name)>4 :
                            ops = row.operator('qv.select', text = vg.name.split('_')[1])
                            ops.vertex_group_index = i
            if context.collection :
                row=layout.row()
                row.label(text = 'Current collection :')
                row=layout.row()
                row.label(text = 'In front : ')
                ops = row.operator('ctrl.in_front_toggle', text = 'ON')
                ops.mode = True
                ops.select = 'COLLECTION'
                ops = row.operator('ctrl.in_front_toggle', text = 'OFF')
                ops.mode = False
                ops.select = 'COLLECTION'

              
            
class RIGSETUP_UL_QS(bpy.types.UIList):
    def draw_item(self, _context, layout, _data, item, icon, _active_data, _active_propname, _index):     
        if self.layout_type in {'DEFAULT', 'COMPACT'}:
            row = layout.row(align=True)
            row.prop(item, "name", text='', emboss=False, icon_value=icon) 
        elif self.layout_type == 'GRID':
            layout.alignment = 'CENTER'
            layout.prop(item, "name", text="", emboss=False, icon_value=icon)    

class EWI_RIG_AddonPref(bpy.types.AddonPreferences):
    bl_idname = __name__
    enable_setup : bpy.props.BoolProperty(default = True)


    list = []

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        row = layout.row()
        row.prop(self, 'enable_setup', text = 'Enable Setup panel')
        row = layout.row()
        row.label(text='Keymap :')
        row = layout.row()
        kc = bpy.context.window_manager.keyconfigs.addon
        col = layout.column()
        for km, kmi in addon_keymaps:
            km = km.active()
            col.context_pointer_set("keymap", km)
            rna_keymap_ui.draw_kmi([], kc, km, kmi, col, 0)



addon_keymaps = []           
default_keymaps = [
    {"name":"ewi.sel_childs", "default_km":"B", "view":"3D View", "space_type" : 'VIEW_3D', 'alt' : True},
    {"name":"ewi.sel_parent", "default_km":"B", "view":"3D View", "space_type" : 'VIEW_3D', 'alt' : False},
    {"name":"ikfk.toggle", "default_km":"K", "view":"3D View", "space_type" : 'VIEW_3D', 'alt' : False},
    ]

cls = [
       RIGSETUP_PT_PANEL,
       RIGSETUP_IKPART, 
       IKSET_OT_ADDPART,
       IKSET_OT_DELPART,
       RIGCONTROL_PT_PANEL,
       IKFK_OT_TOGGLE,
       RIGCTRL_OT_SEL_PARENT,
       FREE_OT_TOGGLE,
       CTRLVIS_OT_TOGGLE,
       OBJECT_OT_FLIP,
       OBJECT_OT_IKFLIP,
       IKSET_OT_FINISH,
       EWI_RIG_AddonPref,
       RIGSETUP_QS,
       RIGSETUP_QUICK_SELS,
       RIGSETUP_OT_ADD_QS,
       RIGSETUP_OT_ADDSEL_QS,
       QS_OT_SELECT,
       RIGSETUP_UL_QS,
       QS_OT_ADD_MENU,
       RIGSETUP_OT_REMOVE_QS,
       IKFK_OT_CLEAR,
       RIGSETUP_OT_SHOW_QS,
       QV_OT_SELECT,
       RIGCTRL_OT_SEL_CHILDS,
       RIGCTRL_OT_SEL,
       ALLCTRLVIS_OT_TOGGLE,
       INFRONT_OT_TOGGLE
       
       ]
def register():
    for cl in cls :
        bpy.utils.register_class(cl)
    bpy.types.Object.ik_parts = bpy.props.CollectionProperty(type = RIGSETUP_IKPART )
    bpy.types.Object.ik_root = bpy.props.PointerProperty(type = bpy.types.Object)
    bpy.types.Object.quick_selects = bpy.props.PointerProperty(type = RIGSETUP_QUICK_SELS)
    bpy.types.Scene.show_qs_on = bpy.props.BoolProperty()
    bpy.types.Scene.show_all_ctrl = bpy.props.BoolProperty()


    
    # Keymapping
    for default_keymap in default_keymaps:
        kc = bpy.context.window_manager.keyconfigs.addon
        if kc :
            
            km = kc.keymaps.new(default_keymap['view'], space_type = default_keymap['space_type'])
            kmi = km.keymap_items.new(default_keymap['name'], type = default_keymap['default_km'], value = 'PRESS', alt = default_keymap['alt'])
            kmi.active = True
            addon_keymaps.append((km, kmi))

def unregister():

    # removing keymaps:
    for km, kmi in addon_keymaps:
        km.keymap_items.remove(kmi)
    addon_keymaps.clear()

    for cl in cls :       
        bpy.utils.unregister_class(cl)
    
    del bpy.types.Object.ik_parts
    del bpy.types.Object.ik_root
    del bpy.types.Object.quick_selects

    
    

    

