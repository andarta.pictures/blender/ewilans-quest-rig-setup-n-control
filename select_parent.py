import bpy 

def get_constraint_children(object, objects_list):
    '''
    returns a list of all the objects in the dependancy tree (childrens, 'child of' constraints targeting the object) recursively
    args : object = the object you wan't to get childrens from
           objects_list = the list where you want to look for children (basically bpy.context.scene)
           ctrl_list = provide an empty list, this is to avoid infinite recursions
    '''
    childs = [] 
    for child in object.children :
        if child not in childs :
            childs.append(child)  
            
    for ob in objects_list :
        if ob not in childs and len([constraint for constraint in ob.constraints 
                                     if constraint.type == 'CHILD_OF' 
                                     and constraint.target is not None
                                     and constraint.target == object
                                     and constraint.influence == 1]) > 0 :
            if ob.name.endswith('_FLIP') :
                flip_childs = get_constraint_children(ob, objects_list)
                for flip_child in flip_childs :
                    if flip_child not in childs :
                        childs.append(flip_child)
            else :
                childs.append(ob)
    return childs

def get_constraint_parent(ob):
    parent = None
    for c in ob.constraints :
        if c.type == 'CHILD_OF' and c.influence == 1 :
            parent = c.target
            break
    if parent and parent.name.endswith('_FLIP') :
        parent = get_constraint_parent(parent)
    return parent

class RIGCTRL_OT_SEL_PARENT(bpy.types.Operator):
    bl_idname = "ewi.sel_parent"
    bl_label = "switch active object to parent"
    bl_options = {'UNDO'}
    def execute(self, context):
        ob = context.object
        parent = get_constraint_parent(ob)

        if parent is None :
            self.report({'INFO'}, 'no parent found')
            return {'CANCELLED'}
        else  : 
            for obj in bpy.context.selected_objects:
                obj.select_set(False)
            parent.select_set(True, view_layer = context.view_layer)
            context.view_layer.objects.active = parent
        return {'FINISHED'} 

#def get_enum_items(self, context):
#    #print('get enum items')
#    ob = context.object
#    childs = get_constraint_children(ob, context.scene.objects)
#    result = [(c.name, c.name,'', i) for i, c in enumerate(childs)]
#    #print(result)
#    return result

class RIGCTRL_OT_SEL_CHILDS(bpy.types.Operator):
    bl_idname = "ewi.sel_childs"
    bl_label = "switch active object to one of his childrens"
    bl_options = {'UNDO'}
    childs = []
    #childs_enum : bpy.props.EnumProperty(items = get_enum_items)

    def invoke(self,context, event):
        #print('invoke')
        ob = context.object
        self.childs = get_constraint_children(ob, context.scene.objects)
        #print(self.childs)
        if len(self.childs) == 0 :
            self.report ({'INFO'},'No children found')
            return {'CANCELLED'}
        elif len(self.childs) == 1 :
            child = self.childs[0]
            for obj in bpy.context.selected_objects:
                obj.select_set(False)
                child.select_set(True, view_layer = context.view_layer)
                context.view_layer.objects.active = child
            return {'FINISHED'}
        else :
            #self.enum =  [(c.name, c.name, i) for i, c in enumerate(self.childs)]
            #print(self.enum)
            wm = context.window_manager  
            #wm.invoke_search_popup(self)
            return  wm.invoke_props_dialog(self) #{'FINISHED'}

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        for child in self.childs :
            row = layout.row()
            ops = row.operator('ewi.sel',text = child.name)
            ops.ob_id = child.name
        #row.prop(self, 'childs_enum', text = 'Asset type ')

    def execute(self, context):
        #child = bpy.data.objects[self.childs_enum]
        #for obj in bpy.context.selected_objects:
        #    obj.select_set(False)
        #    child.select_set(True, view_layer = context.view_layer)
        #    context.view_layer.objects.active = child
        return {'FINISHED'} 
    
class RIGCTRL_OT_SEL(bpy.types.Operator):
    bl_idname = "ewi.sel"
    bl_label = "switch active object"
    bl_options = {'UNDO'}
    ob_id : bpy.props.StringProperty() 
    def execute(self, context):
        ob = bpy.data.objects[self.ob_id]
        
        for obj in bpy.context.selected_objects:
            obj.select_set(False)
        ob.select_set(True, view_layer = context.view_layer)
        context.view_layer.objects.active = ob
        
        return {'FINISHED'} 
